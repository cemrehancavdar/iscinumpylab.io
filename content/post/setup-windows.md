---
title: "Setup Windows"
date: 2022-12-12T11:54:16-0500
categories:
  - Windows
tags:
  - windows
  - setup
---

I set up an ARM DevKit recently, so I wanted to catalog what I do to setup
Windows here. This is not as polished or complete as my macOS setups, because
this is not something I use as a daily driver. But it might be useful, and I'm
open to suggestions to improve it. This should work for Intel or ARM Windows.

Also see [macOS](/post/setup-a-new-mac) [(AS)](/post/setup-apple-silicon).

<!--more-->

## Basic setup

First, update as far as you can. You should always be on the latest feature
release. Microsoft has _drastically_ improved development in the latest
versions. The App Installer is now included (though you should update it), and
Windows Terminal is available without a separate install in Windows 11 and can
be set as the default terminal app.

### Store

First you need to get a few downloads from the Windows Store. You'll want the
latest MSVC and the App Installer. Once you have the installer, you can use the
`winget` command and ignore GUI's. Except for popups during installations.

### WinGet

Now, install some useful tools using the Terminal. If you are not in an
Administrator shell, you'll get popups asking you to elevate privileges to
install (which is fine).

```powershell
winget install python
winget install vim.vim
winget install git.git
winget install kitware.cmake
winget install github.cli
winget install microsoft.visualstudiocode
```

PS: I think you can skip the store section, and install
`VisualStudio.2022.Community` here as well. Also in older versions of Windows, )

### Paths

To actually make Windows usable, you need to add some paths to your `%PATH%`.
Type `env` into the search box and select the "edit system environment
variables" entry. Click on `PATH` and hit edit, then hit new to add a new entry.
Add `C:\Program Files\Vim\vim90`. The other things you've installed above work
correctly without adding anything.

This partially reduces the need to run the various "cmd/powershell" launcher
scripts every Windows app seems to come with. MSVC still has a useful launcher.

You have to restart the terminal to get the path updates into your environment.

### GitHub

Use `gh auth login` to automate the procedure of setting up an ssh key.

### Pipx

Use `pip install pipx` to prepare pipx. It should print a message about script
being installed on a directory that's not on the path - feel free to copy that
into your path (in setting), or use `python -m pipx` instead of `pipx`. Run
`python -m pipx ensurepath` to place pipx's path to your `PATH`.

```python
pip install pipx
python -m pipx ensurepath
```

The second line will also ensure pip's script path is on the path (since that's
where pipx is).

With pipx:

```powershell
pipx install nox
```

(and anything else you need, like pre-commit, tox, etc.).
