{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I've been working on a color addition to Plumbum for a little while, and I'd like to share the basics of using it with you now. This library was originally built around a special `str` subclass, but now is built on the new `Styles` representation and is far more powerful than the first implementation. It safely does nothing if you do not have a color-compatible systems (posix + tty currently), but can be forced if need be. It is included with Plumbum, so you don't have to add a requirement for your scripts that is non-essential (as color often is). It is integrated with `plumbum.cli`, too. Also, I've managed to accelerate the color selection algorithims about 8x, allowing near game-like speeds. (see the `fullcolor.py` example)."
   ]
  },
  {
   "cell_type": "raw",
   "metadata": {},
   "source": [
    "<!--more-->"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Note: The `plumbum.colors` library was written for the terminal and ANSI escape sequences. However, the following article was written in the IPython notebook, so it cannot show ANSI escapes. Due to the fact that the `plumbum.colors` library uses a flexible `Styles` based representation, HTML is easy to implement as a Styles subclass and is available as htmlcolors (see docs for an example). With the `plumbum.colorlib` IPython extension, IPython loads the `%%to html` magic and makes `plumbum.colorlib.htmlcolors` available as `colors`. `htmlcolors` does not support `colors.reset` (and therefore using `colors` directly as a context manager, as well), but otherwise it is very similar. If we are constructing strings, we'll need to remember to include HTML line breaks, so we'll redefine print too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "%load_ext plumbum.colorlib\n",
    "from functools import partial\n",
    "\n",
    "print = partial(print, end=\"<br/>\\n\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Plumbum.colors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Safe color manipulations made easy\n",
    "\n",
    "This is a quick overview and tutorial on the `plumbum.colors` library that is being proposed for Plumbum. It allows you to do things like this:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#0000C0\">This is in Blue.<br/>\n",
       "</font>But this is not.<br/>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "with colors.blue:\n",
    "    print(\"This is in Blue.\")\n",
    "print(\"But this is not.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It works through the COLOR object, which gives you access to styles, and the terminal colors through the forground and background objects. You can wrap a string with the `|` operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#C00000\">This is in red</font> ... but not this<br/>\n",
       "<span style=\"background-color: #FFFF00\">You can change the background too!</span><br/>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "print(colors.red | \"This is in red\", \"... but not this\")\n",
    "print(\"You can change the background too!\" | colors.bg.light_yellow)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The color can go on either side of the string you are wrapping.\n",
    "\n",
    "Styles are available, too:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#00C000\"><span style=\"text-decoration: underline;\">This is underlined</span> and this is still green!<br/>\n",
       "</font>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "with colors.green:\n",
    "    print(colors.underline | \"This is underlined\", \"and this is still green!\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can combine styles, and the result is still a valid style:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<span style=\"background-color: #00FF00\"><font color=\"#C00000\"><b><i>This is a muddle of styles!</i></b></font></span><br/>\n",
       "<font color=\"#C00000\"><s>Twin styles<br/>\n",
       "</s></font>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "mix = colors.bold & colors.italics & colors.red & colors.bg.light_green\n",
    "print(mix | \"This is a muddle of styles!\")\n",
    "with (colors.strikeout & colors.red):\n",
    "    print(\"Twin styles\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "All the major ANSI represetations are supported, include Basic (the first 8 colors), Simple (the first 16 colors), Full (256 colors using three parameter color codes), and True (24 bit color, using 5 parameter color codes). You can even find the closest color in a lower representation if you need to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#000087\">This is from the extended color set.</font><br/>\n",
       "<font color=\"#00AFAF\">And another one.</font><br/>\n",
       "<font color=\"#C129D2\">This supports all colors!</font><br/>\n",
       "<font color=\"#3AB227\">Hex notation, too.</font><br/>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "print(colors.dark_blue | \"This is from the extended color set.\")\n",
    "print(colors[\"LIGHT_SEA_GREEN\"] | \"And another one.\")\n",
    "print(colors.rgb(193, 41, 210) | \"This supports all colors!\")\n",
    "print(colors[\"#3AB227\"] | \"Hex notation, too.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The full list is on the `plumbum.colors` ReadTheDocs page."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a quick shortcut, you use `.print` directly on color (`.print_` if you are using the classic print statment in Python 2):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#D75FD7\">This is in orchid.</font><br/>\n",
       "<span style=\"background-color: #C000C0\">This is on a magenta background.</span><br/>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "colors.orchid.print(\"This is in orchid.\")\n",
    "colors.bg.magenta.print(\"This is on a magenta background.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The colors can be iterated and sliced:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#000000\">This is color:</font> BLACK<br/>\n",
       "<font color=\"#C00000\">This is color:</font> RED<br/>\n",
       "<font color=\"#00C000\">This is color:</font> GREEN<br/>\n",
       "<font color=\"#C0C000\">This is color:</font> YELLOW<br/>\n",
       "<font color=\"#0000C0\">This is color:</font> BLUE<br/>\n",
       "<font color=\"#C000C0\">This is color:</font> MAGENTA<br/>\n",
       "<font color=\"#00C0C0\">This is color:</font> CYAN<br/>\n",
       "<font color=\"#C0C0C0\">This is color:</font> LIGHT_GRAY<br/>\n",
       "<font color=\"#808080\">This is color:</font> DARK_GRAY<br/>\n",
       "<font color=\"#FF0000\">This is color:</font> LIGHT_RED<br/>\n",
       "<font color=\"#00FF00\">This is color:</font> LIGHT_GREEN<br/>\n",
       "<font color=\"#FFFF00\">This is color:</font> LIGHT_YELLOW<br/>\n",
       "<font color=\"#0000FF\">This is color:</font> LIGHT_BLUE<br/>\n",
       "<font color=\"#FF00FF\">This is color:</font> LIGHT_MAGENTA<br/>\n",
       "<font color=\"#00FFFF\">This is color:</font> LIGHT_CYAN<br/>\n",
       "<font color=\"#FFFFFF\">This is color:</font> WHITE<br/>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "for color in colors.fg[:16]:\n",
    "    print(color | \"This is color:\", color.fg.name.upper())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#000000\">&#x25a0;</font> <font color=\"#C00000\">&#x25a0;</font> <font color=\"#00C000\">&#x25a0;</font> <font color=\"#C0C000\">&#x25a0;</font> <font color=\"#0000C0\">&#x25a0;</font> <font color=\"#C000C0\">&#x25a0;</font> <font color=\"#00C0C0\">&#x25a0;</font> <font color=\"#C0C0C0\">&#x25a0;</font> <font color=\"#808080\">&#x25a0;</font> <font color=\"#FF0000\">&#x25a0;</font> <font color=\"#00FF00\">&#x25a0;</font> <font color=\"#FFFF00\">&#x25a0;</font> <font color=\"#0000FF\">&#x25a0;</font> <font color=\"#FF00FF\">&#x25a0;</font> <font color=\"#00FFFF\">&#x25a0;</font> <font color=\"#FFFFFF\">&#x25a0;</font> <font color=\"#000000\">&#x25a0;</font> <font color=\"#00005F\">&#x25a0;</font> <font color=\"#000087\">&#x25a0;</font> <font color=\"#0000AF\">&#x25a0;</font> <font color=\"#0000D7\">&#x25a0;</font> <font color=\"#0000FF\">&#x25a0;</font> <font color=\"#005F00\">&#x25a0;</font> <font color=\"#005F5F\">&#x25a0;</font> <font color=\"#005F87\">&#x25a0;</font> <font color=\"#005FAF\">&#x25a0;</font> <font color=\"#005FD7\">&#x25a0;</font> <font color=\"#005FFF\">&#x25a0;</font> <font color=\"#008700\">&#x25a0;</font> <font color=\"#00875F\">&#x25a0;</font> <font color=\"#008787\">&#x25a0;</font> <font color=\"#0087AF\">&#x25a0;</font> <font color=\"#0087D7\">&#x25a0;</font> <font color=\"#0087FF\">&#x25a0;</font> <font color=\"#00AF00\">&#x25a0;</font> <font color=\"#00AF5F\">&#x25a0;</font> <font color=\"#00AF87\">&#x25a0;</font> <font color=\"#00AFAF\">&#x25a0;</font> <font color=\"#00AFD7\">&#x25a0;</font> <font color=\"#00AFFF\">&#x25a0;</font> <font color=\"#00D700\">&#x25a0;</font> <font color=\"#00D75F\">&#x25a0;</font> <font color=\"#00D787\">&#x25a0;</font> <font color=\"#00D7AF\">&#x25a0;</font> <font color=\"#00D7D7\">&#x25a0;</font> <font color=\"#00D7FF\">&#x25a0;</font> <font color=\"#00FF00\">&#x25a0;</font> <font color=\"#00FF5F\">&#x25a0;</font> <font color=\"#00FF87\">&#x25a0;</font> <font color=\"#00FFAF\">&#x25a0;</font> <font color=\"#00FFD7\">&#x25a0;</font> <font color=\"#00FFFF\">&#x25a0;</font> <font color=\"#5F0000\">&#x25a0;</font> <font color=\"#5F005F\">&#x25a0;</font> <font color=\"#5F0087\">&#x25a0;</font> <font color=\"#5F00AF\">&#x25a0;</font> <font color=\"#5F00D7\">&#x25a0;</font> <font color=\"#5F00FF\">&#x25a0;</font> <font color=\"#5F5F00\">&#x25a0;</font> <font color=\"#5F5F5F\">&#x25a0;</font> <font color=\"#5F5F87\">&#x25a0;</font> <font color=\"#5F5FAF\">&#x25a0;</font> <font color=\"#5F5FD7\">&#x25a0;</font> <font color=\"#5F5FFF\">&#x25a0;</font> <font color=\"#5F8700\">&#x25a0;</font> <font color=\"#5F875F\">&#x25a0;</font> <font color=\"#5F8787\">&#x25a0;</font> <font color=\"#5F87AF\">&#x25a0;</font> <font color=\"#5F87D7\">&#x25a0;</font> <font color=\"#5F87FF\">&#x25a0;</font> <font color=\"#5FAF00\">&#x25a0;</font> <font color=\"#5FAF5F\">&#x25a0;</font> <font color=\"#5FAF87\">&#x25a0;</font> <font color=\"#5FAFAF\">&#x25a0;</font> <font color=\"#5FAFD7\">&#x25a0;</font> <font color=\"#5FAFFF\">&#x25a0;</font> <font color=\"#5FD700\">&#x25a0;</font> <font color=\"#5FD75F\">&#x25a0;</font> <font color=\"#5FD787\">&#x25a0;</font> <font color=\"#5FD7AF\">&#x25a0;</font> <font color=\"#5FD7D7\">&#x25a0;</font> <font color=\"#5FD7FF\">&#x25a0;</font> <font color=\"#5FFF00\">&#x25a0;</font> <font color=\"#5FFF5F\">&#x25a0;</font> <font color=\"#5FFF87\">&#x25a0;</font> <font color=\"#5FFFAF\">&#x25a0;</font> <font color=\"#5FFFD7\">&#x25a0;</font> <font color=\"#5FFFFF\">&#x25a0;</font> <font color=\"#870000\">&#x25a0;</font> <font color=\"#87005F\">&#x25a0;</font> <font color=\"#870087\">&#x25a0;</font> <font color=\"#8700AF\">&#x25a0;</font> <font color=\"#8700D7\">&#x25a0;</font> <font color=\"#8700FF\">&#x25a0;</font> <font color=\"#875F00\">&#x25a0;</font> <font color=\"#875F5F\">&#x25a0;</font> <font color=\"#875F87\">&#x25a0;</font> <font color=\"#875FAF\">&#x25a0;</font> <font color=\"#875FD7\">&#x25a0;</font> <font color=\"#875FFF\">&#x25a0;</font> <font color=\"#878700\">&#x25a0;</font> <font color=\"#87875F\">&#x25a0;</font> <font color=\"#878787\">&#x25a0;</font> <font color=\"#8787AF\">&#x25a0;</font> <font color=\"#8787D7\">&#x25a0;</font> <font color=\"#8787FF\">&#x25a0;</font> <font color=\"#87AF00\">&#x25a0;</font> <font color=\"#87AF5F\">&#x25a0;</font> <font color=\"#87AF87\">&#x25a0;</font> <font color=\"#87AFAF\">&#x25a0;</font> <font color=\"#87AFD7\">&#x25a0;</font> <font color=\"#87AFFF\">&#x25a0;</font> <font color=\"#87D700\">&#x25a0;</font> <font color=\"#87D75F\">&#x25a0;</font> <font color=\"#87D787\">&#x25a0;</font> <font color=\"#87D7AF\">&#x25a0;</font> <font color=\"#87D7D7\">&#x25a0;</font> <font color=\"#87D7FF\">&#x25a0;</font> <font color=\"#87FF00\">&#x25a0;</font> <font color=\"#87FF5F\">&#x25a0;</font> <font color=\"#87FF87\">&#x25a0;</font> <font color=\"#87FFAF\">&#x25a0;</font> <font color=\"#87FFD7\">&#x25a0;</font> <font color=\"#87FFFF\">&#x25a0;</font> <font color=\"#AF0000\">&#x25a0;</font> <font color=\"#AF005F\">&#x25a0;</font> <font color=\"#AF0087\">&#x25a0;</font> <font color=\"#AF00AF\">&#x25a0;</font> <font color=\"#AF00D7\">&#x25a0;</font> <font color=\"#AF00FF\">&#x25a0;</font> <font color=\"#AF5F00\">&#x25a0;</font> <font color=\"#AF5F5F\">&#x25a0;</font> <font color=\"#AF5F87\">&#x25a0;</font> <font color=\"#AF5FAF\">&#x25a0;</font> <font color=\"#AF5FD7\">&#x25a0;</font> <font color=\"#AF5FFF\">&#x25a0;</font> <font color=\"#AF8700\">&#x25a0;</font> <font color=\"#AF875F\">&#x25a0;</font> <font color=\"#AF8787\">&#x25a0;</font> <font color=\"#AF87AF\">&#x25a0;</font> <font color=\"#AF87D7\">&#x25a0;</font> <font color=\"#AF87FF\">&#x25a0;</font> <font color=\"#AFAF00\">&#x25a0;</font> <font color=\"#AFAF5F\">&#x25a0;</font> <font color=\"#AFAF87\">&#x25a0;</font> <font color=\"#AFAFAF\">&#x25a0;</font> <font color=\"#AFAFD7\">&#x25a0;</font> <font color=\"#AFAFFF\">&#x25a0;</font> <font color=\"#AFD700\">&#x25a0;</font> <font color=\"#AFD75F\">&#x25a0;</font> <font color=\"#AFD787\">&#x25a0;</font> <font color=\"#AFD7AF\">&#x25a0;</font> <font color=\"#AFD7D7\">&#x25a0;</font> <font color=\"#AFD7FF\">&#x25a0;</font> <font color=\"#AFFF00\">&#x25a0;</font> <font color=\"#AFFF5F\">&#x25a0;</font> <font color=\"#AFFF87\">&#x25a0;</font> <font color=\"#AFFFAF\">&#x25a0;</font> <font color=\"#AFFFD7\">&#x25a0;</font> <font color=\"#AFFFFF\">&#x25a0;</font> <font color=\"#D70000\">&#x25a0;</font> <font color=\"#D7005F\">&#x25a0;</font> <font color=\"#D70087\">&#x25a0;</font> <font color=\"#D700AF\">&#x25a0;</font> <font color=\"#D700D7\">&#x25a0;</font> <font color=\"#D700FF\">&#x25a0;</font> <font color=\"#D75F00\">&#x25a0;</font> <font color=\"#D75F5F\">&#x25a0;</font> <font color=\"#D75F87\">&#x25a0;</font> <font color=\"#D75FAF\">&#x25a0;</font> <font color=\"#D75FD7\">&#x25a0;</font> <font color=\"#D75FFF\">&#x25a0;</font> <font color=\"#D78700\">&#x25a0;</font> <font color=\"#D7875F\">&#x25a0;</font> <font color=\"#D78787\">&#x25a0;</font> <font color=\"#D787AF\">&#x25a0;</font> <font color=\"#D787D7\">&#x25a0;</font> <font color=\"#D787FF\">&#x25a0;</font> <font color=\"#D7AF00\">&#x25a0;</font> <font color=\"#D7AF5F\">&#x25a0;</font> <font color=\"#D7AF87\">&#x25a0;</font> <font color=\"#D7AFAF\">&#x25a0;</font> <font color=\"#D7AFD7\">&#x25a0;</font> <font color=\"#D7AFFF\">&#x25a0;</font> <font color=\"#D7D700\">&#x25a0;</font> <font color=\"#D7D75F\">&#x25a0;</font> <font color=\"#D7D787\">&#x25a0;</font> <font color=\"#D7D7AF\">&#x25a0;</font> <font color=\"#D7D7D7\">&#x25a0;</font> <font color=\"#D7D7FF\">&#x25a0;</font> <font color=\"#D7FF00\">&#x25a0;</font> <font color=\"#D7FF5F\">&#x25a0;</font> <font color=\"#D7FF87\">&#x25a0;</font> <font color=\"#D7FFAF\">&#x25a0;</font> <font color=\"#D7FFD7\">&#x25a0;</font> <font color=\"#D7FFFF\">&#x25a0;</font> <font color=\"#FF0000\">&#x25a0;</font> <font color=\"#FF005F\">&#x25a0;</font> <font color=\"#FF0087\">&#x25a0;</font> <font color=\"#FF00AF\">&#x25a0;</font> <font color=\"#FF00D7\">&#x25a0;</font> <font color=\"#FF00FF\">&#x25a0;</font> <font color=\"#FF5F00\">&#x25a0;</font> <font color=\"#FF5F5F\">&#x25a0;</font> <font color=\"#FF5F87\">&#x25a0;</font> <font color=\"#FF5FAF\">&#x25a0;</font> <font color=\"#FF5FD7\">&#x25a0;</font> <font color=\"#FF5FFF\">&#x25a0;</font> <font color=\"#FF8700\">&#x25a0;</font> <font color=\"#FF875F\">&#x25a0;</font> <font color=\"#FF8787\">&#x25a0;</font> <font color=\"#FF87AF\">&#x25a0;</font> <font color=\"#FF87D7\">&#x25a0;</font> <font color=\"#FF87FF\">&#x25a0;</font> <font color=\"#FFAF00\">&#x25a0;</font> <font color=\"#FFAF5F\">&#x25a0;</font> <font color=\"#FFAF87\">&#x25a0;</font> <font color=\"#FFAFAF\">&#x25a0;</font> <font color=\"#FFAFD7\">&#x25a0;</font> <font color=\"#FFAFFF\">&#x25a0;</font> <font color=\"#FFD700\">&#x25a0;</font> <font color=\"#FFD75F\">&#x25a0;</font> <font color=\"#FFD787\">&#x25a0;</font> <font color=\"#FFD7AF\">&#x25a0;</font> <font color=\"#FFD7D7\">&#x25a0;</font> <font color=\"#FFD7FF\">&#x25a0;</font> <font color=\"#FFFF00\">&#x25a0;</font> <font color=\"#FFFF5F\">&#x25a0;</font> <font color=\"#FFFF87\">&#x25a0;</font> <font color=\"#FFFFAF\">&#x25a0;</font> <font color=\"#FFFFD7\">&#x25a0;</font> <font color=\"#FFFFFF\">&#x25a0;</font> <font color=\"#080808\">&#x25a0;</font> <font color=\"#121212\">&#x25a0;</font> <font color=\"#1C1C1C\">&#x25a0;</font> <font color=\"#262626\">&#x25a0;</font> <font color=\"#303030\">&#x25a0;</font> <font color=\"#3A3A3A\">&#x25a0;</font> <font color=\"#444444\">&#x25a0;</font> <font color=\"#4E4E4E\">&#x25a0;</font> <font color=\"#585858\">&#x25a0;</font> <font color=\"#626262\">&#x25a0;</font> <font color=\"#6C6C6C\">&#x25a0;</font> <font color=\"#767676\">&#x25a0;</font> <font color=\"#808080\">&#x25a0;</font> <font color=\"#8A8A8A\">&#x25a0;</font> <font color=\"#949494\">&#x25a0;</font> <font color=\"#9E9E9E\">&#x25a0;</font> <font color=\"#A8A8A8\">&#x25a0;</font> <font color=\"#B2B2B2\">&#x25a0;</font> <font color=\"#BCBCBC\">&#x25a0;</font> <font color=\"#C6C6C6\">&#x25a0;</font> <font color=\"#D0D0D0\">&#x25a0;</font> <font color=\"#DADADA\">&#x25a0;</font> <font color=\"#E4E4E4\">&#x25a0;</font> <font color=\"#EEEEEE\">&#x25a0;</font> "
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "for color in colors:\n",
    "    color.print(\"&#x25a0;\", end=\" \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, you can also use `[]` notation to wrap a color (less convenient, but similar to other methods):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<font color=\"#0000C0\">This is wrapped in blue</font><br/>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "print(colors.blue[\"This is wrapped in blue\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Unsafe color manipulations, too"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sometimes, you will find unsafe manipulations faster than wrapping\n",
    "every string. This can be done with `plumbum.colors`, too.\n",
    "\n",
    "If you are planning unsafe manipulations, you can wrap your code in a context manager that restores color to your terminal. For example,\n",
    "```python\n",
    "with colors:\n",
    "    ...unsafe color operations...\n",
    "```\n",
    "All styles will be restored on leaving the manager. The color will automatically be reset when Python quits, as well, even on an exception, so this is not necessary, but is useful in local code.\n",
    "\n",
    "Also, you can restore or set color instantly using the emergency restore from a terminal:\n",
    "```bash\n",
    "$ python -m plumbum.colorlib\n",
    "```\n",
    "This takes any string that you can use in colors, and without a string, it restores color."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The string representation of a color is the ANSI sequence that would produce the color. If you want to instantly write a color to the terminal, you can call the color without arguments. So, either of the following would change the color to blue without restoring it:\n",
    "```python\n",
    "print(colors.blue, end='')\n",
    "colors.blue()\n",
    "```\n",
    "To get the reset color, you can either use the `.reset` property on a factory or a style, or you can use `~`  (inversion). So, this would be a manual color safe wrapping from unsafe components:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "Before <font color=\"#C00000\">Middle</font> After<br/>\n",
       "Before <font color=\"#0000C0\">Middle</font> After<br/>\n",
       "Before <font color=\"#00C000\">Middle</font> After<br/>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%to html\n",
    "print(\"Before \" + colors.red + \"Middle\" + ~colors.red + \" After\")\n",
    "print(\"Before \" + colors.blue + \"Middle\" + ~colors.fg + \" After\")\n",
    "print(\"Before \" + colors.green + \"Middle\" + colors.fg.reset + \" After\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Details of the Style Factories:\n",
    "Let's look at the contents of a `colors.fg` or `colors.bg` object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'ansi',\n",
       " 'black',\n",
       " 'blue',\n",
       " 'cyan',\n",
       " 'dark_gray',\n",
       " 'full',\n",
       " 'green',\n",
       " 'hex',\n",
       " 'light_blue',\n",
       " 'light_cyan',\n",
       " 'light_gray',\n",
       " 'light_green',\n",
       " 'light_magenta',\n",
       " 'light_red',\n",
       " 'light_yellow',\n",
       " 'magenta',\n",
       " 'red',\n",
       " 'reset',\n",
       " 'rgb',\n",
       " 'simple',\n",
       " 'white',\n",
       " 'yellow'}"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "{x for x in dir(colors.bg) if x[0] != \"_\"}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Notice that the extended colors are not listed, to make completion easier. Also, color access is not case sensitive and ignores underscores.\n",
    "\n",
    "Since the `colors` object looks like a `fg` object, let's only look at the unique contents (`.reset` has a different meaning for `colors`, as it resets the terminal completly instead of just the foreground color, so let's remove it from the `fg` set):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "{'bg',\n",
       " 'bold',\n",
       " 'code',\n",
       " 'contains_colors',\n",
       " 'do_nothing',\n",
       " 'em',\n",
       " 'extract',\n",
       " 'fatal',\n",
       " 'fg',\n",
       " 'filter',\n",
       " 'from_ansi',\n",
       " 'get_colors_from_string',\n",
       " 'highlight',\n",
       " 'info',\n",
       " 'italics',\n",
       " 'li',\n",
       " 'load_stylesheet',\n",
       " 'ol',\n",
       " 'reset',\n",
       " 'stdout',\n",
       " 'strikeout',\n",
       " 'success',\n",
       " 'title',\n",
       " 'underline',\n",
       " 'use_color',\n",
       " 'warn'}"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fg = {x for x in dir(colors.bg) if x[0] != \"_\" and x != \"reset\"}\n",
    "col = {x for x in dir(colors) if x[0] != \"_\"}\n",
    "col - fg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the properties are generated based on the attributes allowed for a style, so HTML has some slight differences here vs. ANSI."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stylesheets\n",
    "\n",
    "A recent addition to colors is stylesheets. Stylesheets allow you to use and create styles based on usage. The default sheet is the following:\n",
    "\n",
    "```python\n",
    "default_styles = dict(\n",
    "    warn=\"fg red\",\n",
    "    title=\"fg cyan underline bold\",\n",
    "    fatal=\"fg red bold\",\n",
    "    highlight=\"bg yellow\",\n",
    "    info=\"fg blue\",\n",
    "    success=\"fg green\",\n",
    "    )\n",
    "```\n",
    "\n",
    "You can load a new sheet or a changed sheet with `colors.load_stylesheet(default_styles)`. The new and changed styles will be accessable just like any normal color."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Bonus\n",
    "\n",
    "The cell magic we've been using is actually a slight upgrade on the following example: \n",
    "\n",
    "We are going to make a quick cell magic for IPython to capture output and render html from it. It's really not hard to make a cell magic in IPython:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from io import StringIO\n",
    "from IPython.display import display_html\n",
    "from contextlib import redirect_stdout\n",
    "from IPython.core.magic import register_cell_magic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "@register_cell_magic\n",
    "def output_html(line, cell):\n",
    "    \"Captures stdout and renders it in the notebook as html.\"\n",
    "    out = StringIO()\n",
    "    with redirect_stdout(out):\n",
    "        exec(cell)\n",
    "    out.seek(0)\n",
    "    display_html(out.getvalue(), raw=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's test this to make sure it works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<p>Wow!</p><br/>\n"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "%%output_html\n",
    "print(\"<p>Wow!</p>\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.4.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
